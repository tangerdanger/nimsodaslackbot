import morelogging as logger

let log = newAsyncFileLogger(filename_tpl = "$appname.$y$MM$dd.log")

export log, info, debug, warn, error, fatal
