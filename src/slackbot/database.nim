from os import fileExists
from db_sqlite import open, exec, DbError, sql, DbConn

const DB_PATH = "/tmp/sodabot.db"

proc getOrBuildDatabase*(): DbConn
let db: DbConn = getOrBuildDatabase()

proc buildDatabase*(db: DbConn, rebuild: bool = false, new_sql: string = ""): DbConn = 
  #[
  # Handle building and rebuilding the database
  ]#

  if rebuild:
    try:
      db.exec(sql"DROP TABLE sodabot")
    except DBError:
      echo "Table does not exist, building"

  try:
    if new_sql.len > 0:
      db.exec(sql(new_sql))
    else:
      db.exec(sql("""
    CREATE TABLE sodabot (
    id INTEGER PRIMARY KEY,
    product_name VARCHAR(254) not null,
    quantity INTEGER not null
    user VARCHAR(254) not null
      )"""))
  except DbError:
    #Table exists
    echo "Table exists"

  return db

proc getOrBuildDatabase*(): DbConn =
  if not fileExists(DB_PATH):
    return buildDatabase(db = open(DB_PATH, "sodabot", "sodabot", "sodabot"))
  else:
    return open(DB_PATH, "sodabot", "sodabot", "sodabot")

export db
