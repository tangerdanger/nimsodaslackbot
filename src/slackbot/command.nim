#TODO: Implement message Queue
#TODO: DSL for adding commands
#TODO: DSL parser for commands
import asyncdispatch
import db_sqlite
import strutils
from database import db

import slackapi

const AT_USER_SYMBOL = "^"
const SODA_HELP = """
Sodabot Help:
=============
/add <product> <quantity>   - Adds a product purchase to the DB
/list <product>       - Lists the purchases for a product
/listall          - Lists all purchases to date
/listuser <user>      - Lists all purchases by user
/listallusers <user>      - Lists all purchases by user
/tell <user> <message>      - Direct Messages user <user> a message <message>
      """

proc rowListToLine(row: Row): string =
  #[
  Formats a row to be a little more user friendly
  ]#
  result = "$#:\t $# by $#\L" % [row[0], row[1], slackUserTable[row[2]].name]

proc rowSumsToLine(row: Row): string =
  result = "$#:\t $#\L" % [row[0], row[1]]

proc rowUserSumsToLine(row: Row): string = 
  result = "$#:\t $#: $#\L" % [slackUserTable[row[0]].name, row[1], row[2]]

proc processDirectMessage*(message: SlackMessage): Future[void] {.async.} =
  #[
  Processed messages directly to the bot
  TODO: Add a message buffer
  ]#
  let command = message.text.split(">")[1].strip()
  if command.startsWith("/"):
    #Wait in between commands so we don't flood
    if not slackUserTable.hasKey(message.user):
      echo "No user found for <$#>" % message.user
      return
    await sleepAsync(1000)
    #Builtin command
    case command.split("/")[1].split(" ")[0]:
    of "help":
      discard sendMessage(rtmConnection, newSlackMessage("message", message.channel, SODA_HELP, slackUser.id))
    of "add":
      db.exec(sql"BEGIN")
      db.exec(sql"INSERT INTO sodabot (product_name, quantity, user) VALUES (?, ?, ?)",
        [command.split("/")[1].split(" ")[1], command.split("/")[1].split(" ")[2], message.user])
      db.exec(sql"COMMIT")
      discard sendMessage(rtmConnection, newSlackMessage("message", message.channel, "Purchase recorded!", slackUser.id))
    of "list":
      let arg = command.split("/")[1].split(" ")[1]
      var outMessage = """
Listing records for $#
Product   Quantity   User
===================
""" % arg
      for row in db.rows(sql"SELECT product_name, quantity, user FROM sodabot WHERE product_name=?", arg):
        outMessage.add(rowListToLine(row))
      discard sendMessage(rtmConnection, newSlackMessage("message", message.channel, outMessage, slackUser.id))
    of "listall":
      var outMessage = """
Listing all records
===================
"""
      for row in db.rows(sql"SELECT product_name, quantity, user FROM sodabot"):
        outMessage.add(rowListToLine(row))
      discard sendMessage(rtmConnection, newSlackMessage("message", message.channel, outMessage, slackUser.id))
    of "listuser":
      #ie /listuser ryanc
      var arg = ""
      if command.contains "@":
        arg = command.split("/")[1].split(" ")[1].strip(chars = {'<', '>', '@'})
      else:
        arg = slackUserTable.findUserByName(command.split("/")[1].split(" ")[1]).id
      var outMessage = """
Listing user $#
Product   Total
===================
""" % slackUserTable[arg].name
      for row in db.rows(sql"""SELECT product_name, SUM(quantity) 
        FROM sodabot 
        WHERE user=?
        GROUP BY product_name""", arg):
        outMessage.add(rowSumsToLine(row))
      discard sendMessage(rtmConnection, newSlackMessage("message", message.channel, outMessage, slackUser.id))
    of "listallusers":
      var outMessage = """
Listing all users 
User   Product   Total
===================
"""
      for row in db.rows(sql"""SELECT user, product_name, SUM(quantity) 
        FROM sodabot 
        GROUP BY user, product_name"""):
        outMessage.add(rowUserSumsToLine(row))
      discard sendMessage(rtmConnection, newSlackMessage("message", message.channel, outMessage, slackUser.id))
    of "tell":
      let targetString = command.split("tell")[^1].strip().split(AT_USER_SYMBOL)[^1]
      let user = slackUserTable.findUserByName(targetString.splitWhiteSpace()[0])
      let outMessage = "$#" % targetString.splitWhiteSpace()[1 .. ^1].join(" ")
      let channel = await rtmConnection.openIMChannelForUser(user)
      assert channel.is_im == true
      discard sendMessage(rtmConnection, newSlackMessage("message", channel.id, outMessage, slackUser.id))
    of "tellchannel":
      let targetString = command.split("tellchannel")[^1].strip().split(AT_USER_SYMBOL)[^1]
      let channel: SlackChannel = slackChannelTable.findChannelByName(targetString.splitWhiteSpace()[0])
      let targetUser: SlackUser = slackUserTable.findUserByName(targetString.splitWhiteSpace()[1])
      let outMessage = "@$# $#" % [targetUser.name, targetString.splitWhiteSpace()[2 .. ^1].join(" ")]
      discard sendMessage(rtmConnection, newSlackMessage("message", channel.id, outMessage, slackUser.id))
    else:
      discard sendMessage(rtmConnection, newSlackMessage("message", message.channel, "Sorry, I don't understand that command.\n" & SODA_HELP , slackUser.id))

export SODA_HELP, AT_USER_SYMBOL
