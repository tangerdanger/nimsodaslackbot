# A bot for slack

from db_sqlite import close, DbConn
from strutils import startsWith, `%`
import asyncdispatch, asyncnet
import parseopt, os

import slackapi

import slackbot/command 
import slackbot/shared
from slackbot/database import buildDatabase, getOrBuildDatabase, db

proc handler() {.noconv.} =
  ## Closes our database connection when we quit
  echo "Close handler"
  db.close()
  quit 0

setControlCHook(handler)

proc serve() {.async.} =
  #[
  # Main callback for processing messages
  ]#
  let slackBotUser = slackUserTable.findUserByName("slackbot")
  while true:
    let message = await rtmConnection.readSlackMessage()
    if message.text.len > 0 and message.hidden == false:
      log.info(message)
      #Parse normal messages here
      if message.user.len > 0 and message.user != slackUser.id and message.user != "USLACKBOT":
        if message.text.startsWith("<@$#>" % slackUser.id):
          #Directed message
          asyncCheck processDirectMessage(message)
        else:
          let outMessage = newSlackMessage("message", message.channel, SODA_HELP, slackUser.id)
          discard sendMessage(rtmConnection, message)

when isMainModule:
  let 
    filename = getAppFilename()
    args = commandLineParams()
  
  var parser = initOptParser(args)
  for kind, key, val in parser.getopt():
    case kind
    of cmdArgument:
      echo "Filename: " & key
    of cmdLongOption, cmdShortOption:
      case key
      of "rebuild-sql":
        echo "Rebuilding SQL table"
        discard db.buildDatabase(rebuild = true)
    of cmdEnd:
      assert(false)

  asyncCheck serve()
  asyncCheck ping()
  runForever()

